
# RFI Chamber Tools
This repository provides capturing and channelization software.

## Install

It is mandatory to build the docker image. Clone the repository

`git clone https://gitlab.mpcdf.mpg.de/mpifr-bdg/rfi-chamber-tools`

and build image

`docker build -t <desired-image-name> rfi-chamber-tools`

## Run docker container
To provide sufficient resources (e.g. high-speed capturing and GPU) inside the docker container, some `docker run` flags must be enabled.
For simplicity, the repository contains a bash script allowing the container to run with the necessary flags.

`bash run_docker.sh -i <desired-image-name> -t <tag-of-the-image> -n <container-name> -m <mount-path>`

### Example (madmax-server)
The madmax server has a dedicated path (`/mnt/md1/data/`) on the bare metal system for capturing the data. The directory is mounted to `/data/` into the docker container. Hence, files and sub-directories placed in the mount directory can be accessed inside the docker.
The command

`bash run_docker.sh -i rfi-chamber-tools -t latest -n fsw_spectro -m /mnt/md1/data/:/data/`

is equal to

`bash run_docker.sh`

as the provided command line arguments are the default values.


## Run capturing

To start the capturing, run the command

`docker exec -it <container-name> python capture_data.py --config <your-config>`

Note that the provided config `<your-config>` must be accessible by the docker container. Either copy the config into the container or use the `-m <moun-path>` option to share a directory with the container.

### Example (madmax-server)
To capture data, a configuration file must be provided. Example configurations can be found in `/mnt/md1/data/FSW_SPECTROMETER/CONFIG/MadMax/`.

`docker exec -it fsw_spectro python capture_data.py --config /data/FSW_SPECTROMETER/CONFIG/MadMax/002_BackgroundScans_50-6000MHz.yaml`

The configuration specifies the outputPath as `/data/MPIfR/20230601/002_Background_50-6000_2_3Hz_12s/`. After the measurement, the data is available from `/mnt/md1/data/MPIfR/20230601/002_Background_50-6000_2_3Hz_12s/YYYYMMDD-HHmmSS/` on the bare metal system.

### Output files of a measurement
Per measurement, three files are generated:
- *.rfi, containing the configuration parameters of the measurement
- *.npy, containing the spectrum
- *.npy.hist, containing a historgram

## Quicklook
The repository provides two simple ways to inspect the acquired and processed data.

### Spectrum
The python script `quicklook.py -i path/to/spectrum/file.npy` plots a spectrum.

### Historgram
The python script `quicklook_hist.py path/to/histogram/file.npy.hist` plots a histogram of the data.
import sys
import numpy as np
import matplotlib.pyplot as plt

ar = np.fromfile(sys.argv[1], dtype="int32")
ar = ar.astype("float32")
bins = np.linspace(0, 2, 1024)
ar[ar==0] = np.nan
plt.plot(bins, ar)
#plt.yscale("log")
plt.ylabel("Count")
plt.xlabel("IQ Magnitude (V)")
plt.tight_layout()
plt.show()

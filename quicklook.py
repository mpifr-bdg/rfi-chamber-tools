#!/usr/bin/env python3

import json
import os
import numpy as np
import matplotlib.pyplot as plt


def plot(ar, max_nchans=2**19):
    fig, ax = plt.subplots(1, 1)

    def on_xlims_change(axes):
        xmin, xmax = ax.get_xlim()
        print("New limits: ({}, {})".format(xmin, xmax))
        for line in ax.lines:
            line.remove()
        tmp = ar[(ar["freq"] >= xmin) & ((ar["freq"] <= xmax))]
        print("Size of data in window: {}".format(tmp.size))
        new_maxchans = max_nchans * ar.size // tmp.size
        new_maxchans = 2**((new_maxchans-1).bit_length())
        new_maxchans = min(new_maxchans, ar.size)
        print("Max nchans: {}".format(new_maxchans))
        window = ar.size // new_maxchans
        window = 2**((window-1).bit_length())
        print("Window size: {}".format(window))
        if window > 1:
            tmp = ar.reshape(ar.size // window, window)
            freqs = tmp["freq"].mean(axis=1)
            tmp = tmp[(freqs >= xmin) & ((freqs <= xmax))]
            freqs = tmp["freq"].mean(axis=1)
            plt.plot(freqs, tmp["power"].min(axis=1), c="b")
            plt.plot(freqs, tmp["power"].max(axis=1), c="b")
            plt.plot(freqs, tmp["power"].mean(axis=1), c="r")
        else:
            plt.plot(tmp["freq"], tmp["power"], c="b")
        plt.draw()

    ax.set_xlabel("Frequency (MHz)")
    ax.set_ylabel("Power (dBm)")
    ax.callbacks.connect('xlim_changed', on_xlims_change)
    ax.set_xlim(ar["freq"].min(), ar["freq"].max())
    plt.show()


def load(fname):
    filestem = fname.strip(".npy").strip(".rfi")
    rfi_fname = "{}.rfi".format(filestem)
    npy_fname = "{}.npy".format(filestem)
    if not os.path.exists(npy_fname):
        raise Exception("File {} not found".format(npy_fname))
    if not os.path.exists(rfi_fname):
        raise Exception("File {} not found".format(rfi_fname))
    with open(rfi_fname, "r") as f:
        header = json.load(f)
    for key, value in header.items():
        print("{}: {}".format(key, value))
    data = np.fromfile(npy_fname, dtype="float32")
    nchans = data.size
    bw = header['Bandwidth in Hz']
    cfreq = header['Center Frequency in Hz']
    if nchans != header['Number of Channels']:
        raise Exception(
            "Inconsistent number of channels between .rfi and .npy files")
    ar = np.recarray(nchans, dtype=[("freq", "float64"), ("power", "float32")])
    ar["freq"] = np.linspace(cfreq-bw/2, cfreq+bw/2, nchans) / 1.0e6
    ar["power"] = data
    return ar

def main(args):
    ar = load(args.i)
    plot(ar)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(
        description='Quick plotting routine for RSSpectrometer data')
    parser.add_argument('-i', metavar='FILE', type=str,
        required=True, help='*.npy RSSpectrometer spectrum (assumed *.rfi file exists)')
    args = parser.parse_args()
    main(args)

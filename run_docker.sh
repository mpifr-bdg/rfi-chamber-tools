#!/bin/bash

########
# Help #
########
Help()
{
  echo "This script executes a docker container"
  echo "options:"
  echo "-h     show this help message"
  echo "-n     name of the docker container"
  echo "-i     name of the docker image"
  echo "-t     docker tag"
  echo "-m     mount directory"
}


DOCKERIMAGE="rfi-chamber-tools"
DOCKERTAG="latest"
DOCKERNAME="fsw_spectro"
MOUNT="/mnt/md1/data/:/data/" # src:dst

# Parse args
while getopts "h:n:i:t:m:" option; do
  case $option in
    h) Help
      exit;;
    n) DOCKERNAME=$OPTARG;;
    i) DOCKERIMAGE=$OPTARG;;
    t) DOCKERTAG=$OPTARG;;
    m) MOUNT=$OPTARG;;
    ?) echo "Error: Invalid option"
      exit;;
  esac
done

RUN_CMD="docker run --name=$DOCKERNAME --rm  \
    --tty \
    --runtime=nvidia
    --net=host
    --pid=host
    --rm
    --privileged=true
    --cap-add=IPC_LOCK
    --cap-add=IPC_OWNER
    --cap-add=SYS_NICE
    --cap-add=CAP_NET_ADMIN
    --cap-add=CAP_NET_BROADCAST
    --cap-add=CAP_SYS_ADMIN
    --cap-add=CAP_NET_RAW
    --device=/dev/infiniband/rdma_cm
    --device=/dev/infiniband/issm0
    --device=/dev/infiniband/ucm0
    --device=/dev/infiniband/umad0
    --device=/dev/infiniband/uverbs0"

if [ "$MOUNT" != "" ]; then
    MOUNT_STR="-v $MOUNT"
    RUN_CMD="$RUN_CMD $MOUNT_STR"
fi

$RUN_CMD -d $DOCKERIMAGE:$DOCKERTAG bash -itc 'bash'
FROM nvidia/cuda:12.3.1-devel-ubuntu20.04
LABEL maintainer="ebarr@mpifr-bonn.mpg.de"

ENV GCC_ARCHITECTURE "native"
ENV PSRDADA_VERSION bf756866898686065562ac537376cf9d7d1b54ee
ENV SPEAD2_TAG v3.11.1
ENV MKRECV_TAG 65a520d21bd7354deac01d7c667b4b49564152ab
ENV MKSEND_TAG 47c064942d3b7f90eb336d6e5701f6b8bd09ee57
ENV PSRDADACPP_TAG 0ebcab35f7bdb705d3bb614db3983670c9a98345

# Suppress debconf warnings
ENV DEBIAN_FRONTEND noninteractive

USER root

COPY ./sources.list / etc/apt/

# Software sources go to /src
# basic setup of installed packages and Melanox drivers
RUN mkdir /src && \
    cd /src && \
    apt-get -y check && \
    apt-get -y update && \
    apt-get install -y apt-utils apt-transport-https software-properties-common locales && \
    apt-get -y update --fix-missing && \
    apt-get -y upgrade && \
    echo en_US.UTF-8 UTF-8 > /etc/locale.gen && \
    echo LANG=en_US.UTF-8 > /etc/default/locale && \
    locale-gen  && \
    apt purge -y python2-minimal && \
    apt -y autoremove && \
    # Install dependencies \
    apt-get --no-install-recommends -y install \
      build-essential \
      autoconf \
      iputils-ping \
      autotools-dev \
      automake \
      autogen \
      libtool \
      csh \
      gcc \
      gfortran \
      wget \
      cmake \
      git \
      libltdl-dev \
      gsl-bin \
      libgsl-dev \
      libgsl23 \
      hwloc \
      libhwloc-dev \
      pkg-config \
      net-tools \
      iproute2 \
      htop \
      kmod \
      ethtool \
      lsof \
      pciutils \
      numactl \
      libboost1.67-all-dev \
      librdmacm-dev \
      ibverbs-utils \
      libibverbs-dev \
      python-is-python3 \
      gnupg


### PSRDADA
RUN cd /src && \
    git clone https://git.code.sf.net/p/psrdada/code psrdada && \
    cd psrdada && \
    git checkout ${PSRDADA_VERSION} && \
    export LDFLAGS=-fPIC && \
    export CFLAGS=-fPIC && \
    ./bootstrap && \
    ./configure --includedir=/usr/local/include/psrdada --with-cuda-dir=/usr/local/cuda/ && \
    make -j 8 && \
    make install

### PSRDADACPP 2 \
RUN cd /src && \
    git clone https://gitlab.mpcdf.mpg.de/mpifr-bdg/psrdada_cpp.git && \
    cd psrdada_cpp/ &&\
    git checkout ${PSRDADACPP_TAG} && \
    cmake -S . -B build/ -DENABLE_CUDA=True -DARCH=${GCC_ARCHITECTURE} -DPSRDADA_INCLUDE_DIR=/usr/local/include/psrdada -DCMAKE_BUILD_TYPE=RELEASE &&\
    make -C build/ -j8 && make -C build/ install

### SPEAD 2 dependencies
RUN   cd /src && \
    apt-get --no-install-recommends -y install \
        python3-jinja2 \
        python3-pycparser \
        python3-packaging \
        libdivide-dev

### SPEAD 2 \
 RUN  cd /src && \
    git clone https://github.com/ska-sa/spead2.git && \
    cd spead2 && \
    git checkout ${SPEAD2_TAG}  && \
    ./bootstrap.sh --no-python && \
    ./configure && \
    make -j8 &&\
    make install

### MKRECV \
RUN  cd /src && \
    git clone https://gitlab.mpifr-bonn.mpg.de/mhein/mkrecv.git && \
    cd mkrecv &&\
    git checkout ${MKRECV_TAG}  && \
    cmake -S . -B build/ -DENABLE_CUDA=TRUE -DPSRDADA_CPP_INCLUDE_DIR=/src/psrdada_cpp/ -DPSRDADA_CPP_LIBRARIES=/src/psrdada_cpp/build/psrdada_cpp/libpsrdada_cpp.a -DPSRDADA_INCLUDE_DIR=/usr/local/include/psrdada -DARCH=${GCC_ARCHITECTURE} && \
    make -C build/ -j8 && make -C build/ install


RUN   cd /src && \
    apt-get --no-install-recommends -y install \
        python3-setuptools \
        python3-setuptools-git \
        python3-setuptools-scm \
        python3-pip \
        python3-coloredlogs \
        python3-yaml \
        &&\
        \
    pip3 install --no-deps \
        astropy==4.0 \
        typing-extensions==4.4.0\
        setuptools_scm==7.0.5 \
        PyVISA==1.14.1 \
        PyVISA-py==0.7.1 \
        numpy==1.17.4 \
        && \
    echo 'DONE!'

# Add library path
RUN echo "/usr/local/lib" >> /etc/ld.so.conf.d/local.conf && ldconfig

RUN cd /src/ && \
    git clone https://gitlab.mpcdf.mpg.de/mpifr-bdg/rfi-chamber-tools

WORKDIR /src/rfi-chamber-tools